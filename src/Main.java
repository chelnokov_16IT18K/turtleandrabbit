/**
 * Класс для создания и запуска объектов кромлика и черепахи
 */
public class Main {
    public static void main(String[] args) {
        Running rabbit = new Running("Заяц", 10);
        Running turtle = new Running("Черепаха", 1);

        rabbit.start();
        turtle.start();
    }
}
