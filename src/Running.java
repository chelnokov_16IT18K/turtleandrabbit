/**
 * Класс для создания "бегающих" потоков
 *
 * @author Chelnokov E.I.
 */
public class Running extends Thread {

    Running(String name, int priority) {
        setName(name);
        setPriority(priority);
    }

    /**
     * Метод запуска потоков
     */
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            if (i % 20_000 == 0) {
                System.out.println(getName() + " пробежал " + i + " метров");
            }
            if (i % 500_000 ==0){//смена приоритета
                if (getPriority() == 10){
                    setPriority(1);
                }else{
                    setPriority(10);
                }
            }
        }
        System.out.println(getName() + " больше не бежит");
    }
}
